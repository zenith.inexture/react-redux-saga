import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FETCH_USERS_REQUEST } from "../redux/dataFetchRedux/dataFetchType";

const UserData = () => {
  const userData = useSelector((state) => state.data.users);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({ type: FETCH_USERS_REQUEST });
    // eslint-disable-next-line
  }, []);
  return (
    <div>
      {userData &&
        userData.map((user) => (
          <React.Fragment key={user.id}>
            <p>{user.id}</p>
            <p>{user.name}</p>
            <hr></hr>
          </React.Fragment>
        ))}
    </div>
  );
};

export default UserData;
