import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  decrementCounter,
  incrementCounter,
  resetCounter,
} from "../redux/counterRedux/counterAction";
import { INCREMENT_A_COUNTER } from "../redux/counterRedux/counterType";

const Counter = () => {
  //fetch the count from store
  const countValue = useSelector((state) => state.counter.count);

  //create instance of useDispatch() hook
  const dispatch = useDispatch();

  return (
    <div>
      <p>{countValue}</p>
      <button onClick={() => dispatch(incrementCounter())}>Increment</button>
      <button onClick={() => dispatch({ type: INCREMENT_A_COUNTER })}>
        Async Increment
      </button>
      <button onClick={() => dispatch(decrementCounter())}>Decrement</button>
      <button onClick={() => dispatch(resetCounter())}>Reset</button>
    </div>
  );
};

export default Counter;
