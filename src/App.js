import Counter from "./components/Counter";
import UserData from "./components/UserData";

function App() {
  return (
    <div>
      <Counter />
      <hr></hr>
      <UserData />
    </div>
  );
}

export default App;
