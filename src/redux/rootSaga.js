import { fork } from "redux-saga/effects";
import watchIncrementCount from "./counterRedux/counterAction";
import watchFetchUser from "./dataFetchRedux/dataFetchAction";

//This is root-saga it's contain all the sagas
// export default function* rootSaga() {
//   yield all([watchIncrementCount(), watchFetchUser()]);
// }
export default function* mySaga() {
  yield fork(watchIncrementCount);
  yield fork(watchFetchUser);
}
