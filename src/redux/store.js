import {
  applyMiddleware,
  combineReducers,
  legacy_createStore as createStore,
} from "redux";
import counterReducer from "./counterRedux/counterReducer";
import createSagaMiddleware from "redux-saga";
import dataFetchReducer from "./dataFetchRedux/dataFetchReducer";
import rootSaga from "./rootSaga";

const rootReducer = combineReducers({
  counter: counterReducer,
  data: dataFetchReducer,
});
// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// mount middleware with the store
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

// then run the generator function in saga
//This is root-saga for the watching on every saga
sagaMiddleware.run(rootSaga);

export default store;
