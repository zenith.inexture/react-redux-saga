import {
  DECREMENT_COUNTER,
  INCREMENT_ASYNC_COUNTER,
  INCREMENT_A_COUNTER,
  INCREMENT_COUNTER,
  RESET_COUNTER,
} from "./counterType";
import { put, takeEvery } from "redux-saga/effects";

/**
 * action for inceremnt the counter
 * @returns object of type
 */
export const incrementCounter = () => {
  return {
    type: INCREMENT_COUNTER,
  };
};

/**
 * action for decrement the counter
 */
export const decrementCounter = () => {
  return {
    type: DECREMENT_COUNTER,
  };
};

/**
 * action for the reseting the counter
 */
export const resetCounter = () => {
  return {
    type: RESET_COUNTER,
  };
};

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* incrementAsyncCounter() {
  yield put({ type: INCREMENT_ASYNC_COUNTER });
}
/**
 * watcher function watch on for the dispath method every time in program
 */
function* watchIncrementCount() {
  yield takeEvery(INCREMENT_A_COUNTER, incrementAsyncCounter);
}

export default watchIncrementCount;
