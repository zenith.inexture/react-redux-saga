import {
  DECREMENT_COUNTER,
  INCREMENT_ASYNC_COUNTER,
  INCREMENT_COUNTER,
  RESET_COUNTER,
} from "./counterType";

// initialState - contain the initial value of the count
const initialState = {
  count: 0,
};

/**
 * for updating the count value in store
 * @param {initialState as state} state
 * @param {action the provide by the redux} action
 * @returns updation in perticular action
 */
const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT_COUNTER:
      return {
        ...state,
        count: state.count + 1,
      };
    case INCREMENT_ASYNC_COUNTER:
      return {
        ...state,
        count: state.count + 1,
      };
    case DECREMENT_COUNTER:
      return {
        ...state,
        count: state.count - 1,
      };
    case RESET_COUNTER:
      return {
        ...state,
        count: initialState.count,
      };
    default:
      return state;
  }
};

export default counterReducer;
