import { call, put, takeEvery } from "redux-saga/effects";
import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
} from "./dataFetchType";

/**
 * that make the api call and fetch the data from api
 * @returns the fetched data
 */
const userFetcher = async () => {
  let response = await fetch("https://jsonplaceholder.typicode.com/users");
  response = await response.json();
  return response;
};

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchUser() {
  try {
    const user = yield call(userFetcher);
    yield put({ type: FETCH_USERS_SUCCESS, payload: user });
  } catch (error) {
    yield put({ type: FETCH_USERS_FAILURE, payload: error.messsage });
  }
}

/**
 * watcher function watch on for the dispath method every time in program
 */
function* watchFetchUser() {
  yield takeEvery(FETCH_USERS_REQUEST, fetchUser);
}

export default watchFetchUser;

//discription
/**
 * call - call is function that take function as
 * yield - yield is allow us to to pause and resume the function asynchronously.
 *         insted of return someting in function, we have to yield form function.
 * takeEvery - this allows to use of several fetchData objects at the same time.
 * takeLetest - takeLatest allows only one fetchData task to run at any moment.
 */
